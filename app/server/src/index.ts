import path from "path";

import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import { graphqlHTTP } from "express-graphql";

import graphqlSchema from "./graphql/schemas/index";
import graphqlResolvers from "./graphql/resolvers/index";

require("dotenv").config({ path: path.resolve(__dirname, ".env") });

const app: express.Express = express();
const PORT = process.env.PORT || 4000;

app.use(cors());
app.use(bodyParser.json());

app.use(
	"/graphql",
	graphqlHTTP({
		schema: graphqlSchema,
		rootValue: graphqlResolvers,
		graphiql: {
			defaultQuery: "# Repo's here >> https://gitlab.com/morelcisse/monorepoc",
		},
	})
);

app.get("/", (req, res) => {
	res.json("Run Cypress tests !");
});

app.listen(PORT, () => console.log(`Server listen on PORT ${PORT}`));
