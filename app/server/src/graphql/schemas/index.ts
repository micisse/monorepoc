import { buildSchema } from "graphql";

const Schema = buildSchema(`
	type UserType {
		email: String!
		firstname: String!
		lastname: String!
		password: String!
		access_token: String
		connected: Boolean
	}

	type MsgType {
		message: String!
		error: Boolean!
	}

	input RegisterInput {
		firstname: String!
		lastname: String!
		email: String!
		password: String!
	}

	union LoginResult = UserType | MsgType
	union MeResult = UserType | MsgType

	input LoginInput {
		email: String
		password: String
	}

	type Query {
		me(access_token: String!): MeResult!
		login(user: LoginInput!): LoginResult!
		logout(access_token: String!): MsgType!
	}

	type Mutation {
		register(user: RegisterInput!): MsgType!
		deleteUser(access_token: String!): MsgType!
	}

	schema {
		query: Query
		mutation: Mutation
	}
`);

export default Schema;
