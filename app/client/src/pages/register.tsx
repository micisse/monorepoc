import { gql } from "@apollo/client";
import { apolloClient } from "common/helpers/Apollo";
import Button from "@material-ui/core/Button";
import Link from "next/link";
import { commonStyles } from "src/styles/main";
import { TextField } from "formik-material-ui";
import { Formik, Form, Field } from "formik";
import CenteredLayout from "src/components/Layout/GridLayout";
import { Box } from "@material-ui/core";
import { IUser } from "common/interfaces/User";
import { useRouter } from "next/router";
import { useState } from "react";
import LinearProgress from "@material-ui/core/LinearProgress";
import { useSnackbar } from "notistack";

const Register = () => {
	const [submited, setSubmited] = useState(false);
	const { enqueueSnackbar } = useSnackbar();
	const classes = commonStyles();
	const router = useRouter();
	const validate = (values: IUser) => {
		const errors: Partial<IUser> = {};

		if (!values.email) {
			errors.email = "Ce champs es requis";
		}
		if (!values.password) {
			errors.password = "Ce champs es requis";
		}
		if (!values.firstname) {
			errors.firstname = "Ce champs es requis";
		}
		if (!values.lastname) {
			errors.lastname = "Ce champs es requis";
		}

		return errors;
	};
	const onSubmit = (values: IUser) => {
		setSubmited(true);
		apolloClient
			.mutate({
				mutation: gql`
					mutation (
						$firstname: String!
						$lastname: String!
						$email: String!
						$password: String!
					) {
						register(
							user: {
								firstname: $firstname
								lastname: $lastname
								email: $email
								password: $password
							}
						) {
							message
							error
						}
					}
				`,
				variables: { ...values },
			})
			.then(({ data }) => {
				const result = data.register;
				const message = result.message;

				enqueueSnackbar(message, {
					variant: result.error ? "error" : "success",
					autoHideDuration: 3000,
				});
				if (!result.error) {
					setTimeout(() => {
						router.push("login", "/login");
					}, 500);
				} else {
					setSubmited(false);
				}
			});
	};

	return (
		<CenteredLayout title="INSCRIVEZ-VOUS">
			{submited && <LinearProgress color="primary" />}
			<Formik
				initialValues={{ email: "", password: "", firstname: "", lastname: "" }}
				validate={validate}
				onSubmit={onSubmit}
			>
				{({ touched, errors }) => (
					<Form className={classes.form}>
						<Field
							component={TextField}
							name="firstname"
							type="firstname"
							id="register-firstname"
							label="Nom"
							fullWidth
							error={touched["firstname"] && !!errors["firstname"]}
							helperText={touched["firstname"] && errors["firstname"]}
							variant="outlined"
							margin="normal"
							disabled={submited}
						/>
						<Field
							component={TextField}
							name="lastname"
							type="lastname"
							id="register-lastname"
							label="Prénom"
							fullWidth
							error={touched["lastname"] && !!errors["lastname"]}
							helperText={touched["lastname"] && errors["lastname"]}
							variant="outlined"
							margin="normal"
							disabled={submited}
						/>
						<br />
						<Field
							component={TextField}
							name="email"
							type="email"
							id="register-email"
							label="Adresse email"
							fullWidth
							error={touched["email"] && !!errors["email"]}
							helperText={touched["email"] && errors["email"]}
							variant="outlined"
							margin="normal"
							disabled={submited}
						/>
						<br />
						<Field
							component={TextField}
							id="register-password"
							type="password"
							label="Mot de passe"
							name="password"
							fullWidth
							error={touched["password"] && !!errors["password"]}
							helperText={touched["password"] && errors["password"]}
							variant="outlined"
							margin="normal"
							disabled={submited}
						/>
						<br />
						<Box className={classes.submitBox}>
							<Button
								type="submit"
								id="register-submit"
								variant="contained"
								color="primary"
								className={classes.submit}
								disabled={submited}
							>
								Inscription
							</Button>
						</Box>
						<br />
						<small className={classes.link}>
							Vous avez déja un compte ?{" "}
							<Link href="login" as="/login">
								Connectez-vous
							</Link>
						</small>
					</Form>
				)}
			</Formik>
		</CenteredLayout>
	);
};

export default Register;
