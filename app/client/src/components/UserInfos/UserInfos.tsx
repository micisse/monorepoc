import { useEffect, useState } from "react";
import { commonStyles } from "src/styles/main";
import { Box, Typography } from "@material-ui/core";
import { gql } from "@apollo/client";
import { apolloClient } from "common/helpers/Apollo";
import { token } from "src/helpers";
import { IUser } from "common/interfaces/User";
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useSnackbar } from "notistack";

const UserInfos = () => {
	const { enqueueSnackbar } = useSnackbar();
	const classes = commonStyles();
	const [loading, setLoading] = useState(true);
	const [user, setUser] = useState<IUser>(null);

	useEffect(() => {
		apolloClient
			.query({
				query: gql`
					query ($access_token: String!) {
						me(access_token: $access_token) {
							... on MsgType {
								message
								error
							}
							... on UserType {
								email
								firstname
								lastname
								access_token
								connected
							}
						}
					}
				`,
				variables: { access_token: token() },
			})
			.then(({ data }) => {
				const result = data.me;

				if (result.__typename === "MsgType" && result.error) {
					return enqueueSnackbar(result.message, { variant: "error" });
				}

				setUser(result);
				setTimeout(() => {
					setLoading(false);
				}, 500);
			});
	}, []);

	return (
		<div className={classes.root}>
			<Typography variant="h4" className={classes.title} gutterBottom>
				INFOS
			</Typography>

			{loading ? (
				<Box display="flex" justifyContent="center">
					<CircularProgress />
				</Box>
			) : (
				<>
					<Grid container spacing={3}>
						<Grid item xs>
							<div className={`firstname-info ${classes.paper}`}>
								<b>NOM</b>
								<br />
								<span className="info-value">{user.firstname}</span>
							</div>
						</Grid>
						<Grid item xs>
							<div className={`lastname-info ${classes.paper}`}>
								<b>PRÉNOM</b>
								<br />
								<span className="info-value">{user.lastname}</span>
							</div>
						</Grid>
					</Grid>

					<Grid container spacing={3}>
						<Grid item xs>
							<div className={`email-info ${classes.paper}`}>
								<b>EMAIL</b>
								<br />
								<span className="info-value">{user.email}</span>
							</div>
						</Grid>
						<Grid item xs>
							<div className={`connected-info ${classes.paper}`}>
								<b>STATUS</b>
								<br />
								<span className="info-value">
									{user.connected ? "Connecté" : ""}
								</span>
							</div>
						</Grid>
					</Grid>
				</>
			)}
		</div>
	);
};

export default UserInfos;
