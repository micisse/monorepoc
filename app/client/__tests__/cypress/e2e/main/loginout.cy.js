/// <reference types="cypress" />

context("Login | Logout", () => {
	const url = "http://localhost:3000";

	beforeEach(() => {
		cy.visit(url);
	});

	it("The user connects and disconnects", () => {
		cy.fixture("users.json").then((u) => {
			const user = u[0];

			cy.get("#login").click();

			cy.wait(100); // for cypress open, you can remove or comment as you want

			cy.url().should("eq", `${url}/login`);

			cy.get("#login-submit").click();

			cy.get("#login-email-helper-text").contains("Ce champs es requis");
			cy.get("#login-password-helper-text").contains("Ce champs es requis");

			cy.wait(600); // for cypress open, you can remove or comment as you want

			cy.get("input[name='email']").type(user.email);
			cy.get("input[name='password']").type(user.password);

			cy.get("#login-email-helper-text").should("not.exist");
			cy.get("#login-password-helper-text").should("not.exist");

			cy.get("#login-submit").click();

			cy.get("#notistack-snackbar").as("alert");
			cy.get("@alert")
				.invoke("text")
				.then((text) => {
					const errors = [
						"Vos identifiants de connexion sont incorrectes",
						"Cet utilisateur n'existe pas",
					];

					if (!errors.includes(text)) {
						expect(text).to.equal("Vous êtes connecté");
						cy.wait(600);
						cy.url().should("eq", `${url}/dashboard`);

						cy.get(".MuiAppBar-root").should("exist");

						cy.getLocalStorage("token").should("exist");
						cy.getLocalStorage("username").should("exist");

						cy.get(".nav-title").should("not.be.empty");

						cy.get(".firstname-info").contains("NOM");
						cy.get(".firstname-info .info-value").should("not.be.empty");
						cy.get(".firstname-info .info-value").contains(user.firstname);

						cy.get(".lastname-info").contains("PRÉNOM");
						cy.get(".lastname-info .info-value").should("not.be.empty");
						cy.get(".lastname-info .info-value").contains(user.lastname);

						cy.get(".email-info").contains("EMAIL");
						cy.get(".email-info .info-value").should("not.be.empty");
						cy.get(".email-info .info-value").contains(user.email);

						cy.get(".connected-info").contains("STATUS");
						cy.get(".connected-info .info-value").should("not.be.empty");
						cy.get(".connected-info .info-value").contains("Connecté");

						/* -------------------------------------------------------------------------- */
						/*                                   LOGOUT                                   */
						/* -------------------------------------------------------------------------- */

						cy.get("#logout").click();
						cy.get("#notistack-snackbar").as("alert");
						cy.get("@alert")
							.invoke("text")
							.then((text) => {
								expect(text).to.equal("Vous êtes déconnecté");
								cy.wait(600);
								cy.url().should("eq", `${url}/`);

								cy.getLocalStorage("token").should("not.exist");
								cy.getLocalStorage("username").should("not.exist");
							});
					} else {
						expect(text).to.satisfy((responseText) => errors.includes(responseText));
						cy.wait(100); // for cypress open, you can remove or comment as you want
						cy.url().should("eq", `${url}/login`);
					}
				});
		});
	});
});
