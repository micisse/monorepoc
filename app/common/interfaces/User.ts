export interface IUser {
	firstname: string;
	lastname: string;
	email: string;
	password: string;
	access_token?: string | null;
	connected?: boolean;
}
